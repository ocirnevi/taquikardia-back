const design = [
  {
    products: {
      id: 0,
      imageName: '',
      name: '',
      price: '',
      days: [],
      startTime: Time(),
      finishTime: Time(),
      showAlways: false,
      isBeverage: false
    },
    orders: {
      id: 0,
      typeId: 0,
      statusId: 0,
      detail: [
        {
          productId: 0,
          quantity: 0,
          price: 0
        }
      ]
    },
    orderTypes: {
      id: 0,
      name: ''
    },
    orderStatuses: {
      id: 0,
      name: ''
    }
  }
]
