/**
 * Products.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    imageName: {
      type: 'string'
    },
    name: {
        type: 'string',
        required: true
    },
    price: {
        type: 'number',
        required: true
    },
    days: {
      type: 'json'
    },
    startTime: {
      type: 'string'
    },
    finishTime: {
      type: 'string'
    },
    showAlways: {
      type: 'boolean',
      defaultsTo: false
    },
    isBeverage: {
      type: 'boolean'
    }
  }
};
