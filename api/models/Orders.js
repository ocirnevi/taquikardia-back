/**
 * Orders.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  products: {
    type: 'json'
  },
  type: {
    type: 'ref'
  },
  status: {
    type: 'ref'
  }
};

